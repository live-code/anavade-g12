import { Component, OnInit } from '@angular/core';
import { TabBarComponent } from '../../shared/tab-bar.component';

interface Country {
  id: number;
  name: string;
  desc: string;
  cities: City[];
}
interface City {
  id: number;
  name: string;
  desc: string;
}

@Component({
  selector: 'ava-demo-uikit2',
  template: `
    <ava-tab-bar 
      [items]="countries" 
      [active]="activeCountry"
      (tabClick)="selectCountryHandler($event)"
    ></ava-tab-bar>

    <ava-tab-bar
      [items]="activeCountry?.cities"
      [active]="activeCity"
      (tabClick)="selectCityHandler($event)"
    ></ava-tab-bar>
    
    <div class="alert alert-info" *ngIf="activeCity">
      <ava-gmap [value]="activeCity?.name"></ava-gmap>
    </div>
    
    
    <ava-tab-bar
      labelField="label"
      [items]="users"></ava-tab-bar>
  `,
})
export class DemoUikit2Component implements OnInit {
  countries: Country[];
  activeCountry: Country;
  activeCity: City;
  users = [
    { id: 1, label: 'Fabio' },
    { id: 2, label: 'Mario' },
    { id: 3, label: 'Pippo' },
  ]

  ngOnInit(): void {
    setTimeout(() => {
      this.countries = [
        {
          id: 1, name: 'Italy', desc: 'bla bla spaghetti e mandolino',
          cities: [
            { id: 10, name: 'Rome', desc: 'Roma bla bla'},
            { id: 11, name: 'Milan', desc: 'Milan bla bla'},
            { id: 12, name: 'Trieste', desc: 'Trieste bla bla'},
          ]
        },
        {
          id: 2, name: 'Germany', desc: 'bla bla wurstel',
          cities: [
            { id: 102, name: 'Berlin', desc: 'Berlin bla bla'},
          ]
        },
        {
          id: 3, name: 'Spain', desc: 'bla bla sangria',
          cities: [
            { id: 130, name: 'Madrid', desc: 'Madrid bla bla'},
            { id: 141, name: 'Barcelona', desc: 'Barcelona bla bla'},
          ]
        },
      ];
      this.selectCountryHandler(this.countries[0])
    }, 2000);
  }

  selectCountryHandler(country: Country): void {
    this.activeCountry = country;
    this.activeCity = this.activeCountry.cities[0];
  }

  selectCityHandler(city: City): void {
    this.activeCity = city;
  }
}


