import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DemoUikit2Component } from './demo-uikit2.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [DemoUikit2Component],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: DemoUikit2Component}
    ])
  ]
})
export class DemoUikit2Module { }
