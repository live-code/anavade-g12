import { Component, OnInit } from '@angular/core';
import { CatalogService } from './services/catalog.service';

@Component({
  selector: 'ava-catalog',
  template: `
    <ava-catalog-errors *ngIf="catalogService.error"></ava-catalog-errors>
    
    <ava-card title="ADD PRODUCT">
      <ava-catalog-form 
        (saveUser)="catalogService.saveHandler($event)"
      ></ava-catalog-form>
    </ava-card>

    <ava-card [title]="catalogService.products | totalProduct">
      <ava-catalog-list 
        [products]="catalogService.products" 
        (deleteUser)="catalogService.deleteHandler($event)"
      ></ava-catalog-list>

      <ava-catalog-total [products]="catalogService.products"></ava-catalog-total>
    </ava-card>
  `,
})
export class CatalogComponent implements OnInit {
  constructor(public catalogService: CatalogService) { }

  ngOnInit(): void {
    this.catalogService.loadProducts();
  }
}

