import { Injectable } from '@angular/core';
import { Product } from '../../../model/product';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { NgForm } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class CatalogService {
  products: Product[];
  error: boolean;

  constructor(private http: HttpClient) { }

  loadProducts(): void {
    this.error = false;
    this.http.get<Product[]>(`${environment.baseUrl}/catalog?_sort=price&_order=asc`)
      .subscribe(
        res => this.products = res,
        err => this.error = true
      );
  }

  saveHandler(f: NgForm): void {
    this.http.post<Product>(`${environment.baseUrl}/catalog`, f.value)
      .subscribe(res => {
        this.products = [...this.products, res];
        f.reset();
      });
  }

  deleteHandler(id: number): void {
    this.error = false;
    this.http.delete(`${environment.baseUrl}/catalog/${id}`)
      .subscribe(
        () => {
          this.products = this.products.filter(p => p.id !== id);
        },
        err => this.error = true
      );
  }
}
