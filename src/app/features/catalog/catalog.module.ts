import { NgModule } from '@angular/core';
import { CatalogComponent } from './catalog.component';
import { CatalogErrorsComponent } from './components/catalog-errors.component';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogListComponent } from './components/catalog-list.component';
import { CatalogTotalComponent } from './components/catalog-total.component';
import { TotalCartPipe } from './pipes/total-cart.pipe';
import { CatalogListItemComponent } from './components/catalog-list-item.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CardComponent } from '../../shared/card.component';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    // catalog module
    CatalogComponent,
    CatalogErrorsComponent,
    CatalogFormComponent,
    CatalogListComponent,
    CatalogTotalComponent,
    TotalCartPipe,
    CatalogListItemComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: CatalogComponent}
    ])
  ]
})
export class CatalogModule {

}
