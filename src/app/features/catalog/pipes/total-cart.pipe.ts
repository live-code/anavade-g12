import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../../../model/product';

@Pipe({
  name: 'totalProduct',
  pure: false
})
export class TotalCartPipe implements PipeTransform {
  transform(items: Product[]): string {
    const total = items?.reduce((acc, curr) => acc + curr.price, 0);
    const plurals = items?.length > 1 ? 'i' : 'o';
    return `€ ${total?.toFixed(2)} (${items?.length} prodott${plurals})`
  }
}

