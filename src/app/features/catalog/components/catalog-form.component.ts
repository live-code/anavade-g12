import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'ava-catalog-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div *ngIf="f.invalid">form invalid</div>
    <form #f="ngForm" (submit)="saveUser.emit(f)">
      <input
        type="text"
        ngModel
        name="name"
        required
        class="form-control"
        placeholder="Product Name"
      >
      <input
        type="number"
        ngModel
        name="price"
        required
        class="form-control"
        placeholder="Cost"
      >
      <button
        type="submit"
        [disabled]="f.invalid">ADD</button>
      <button
        type="button"
        (click)="clearHandler(f)">CLEAR</button>
    </form>
    
  `,
})
export class CatalogFormComponent {
  @Output() saveUser = new EventEmitter<NgForm>();

  clearHandler(f: NgForm): void {
    f.reset();
  }

}
