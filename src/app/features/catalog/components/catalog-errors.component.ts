import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ava-catalog-errors',
  template: `
    <div class="alert alert-danger">
      Error server side
    </div>
  `,
  styles: [
  ]
})
export class CatalogErrorsComponent implements OnInit {
  @Input() visible: boolean;

  ngOnInit(): void {
  }

}
