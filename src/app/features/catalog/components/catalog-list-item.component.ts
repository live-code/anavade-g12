import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from '../../../model/product';

@Component({
  selector: 'ava-catalog-list-item',
  template: `
    <li class="list-group-item">
      {{product.name}} - € {{product.price | number: '1.2-2'}}
      <i class="fa fa-trash pull-right"
         (click)="deleteUserItem.emit(product.id)"></i>

      <i
        class="fa fa-arrow-circle-o-down"
        (click)="isOpen = !isOpen"
      ></i>

      <div *ngIf="isOpen">
        {{product.desc}}
      </div>
    </li>
  `,
  styles: [
  ]
})
export class CatalogListItemComponent {
  @Input() product: Product;
  @Output() deleteUserItem = new EventEmitter<number>();
  isOpen = false;

}
