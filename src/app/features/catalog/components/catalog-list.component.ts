import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../../../model/product';

@Component({
  selector: 'ava-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ava-catalog-list-item
      *ngFor="let product of products"
      [product]="product"
      (deleteUserItem)="deleteUser.emit($event)"
    >
    </ava-catalog-list-item>
  `,
})
export class CatalogListComponent {
  @Input() products: Product[];
  @Output() deleteUser = new EventEmitter<number>();

}
