import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Product } from '../../../model/product';

@Component({
  selector: 'ava-catalog-total',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
     <div class="text-center">
      <span class="badge bg-secondary rounded-pill ">
         {{products | totalProduct}}
      </span>
    </div>
  `,
})
export class CatalogTotalComponent {
  @Input() products: Product[];
}
