import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ava-demo-uikit1',
  template: `
    <ava-card
      title="Profile"
      buttonLabel="Visit Profile"
      (buttonClick)="goto('http://www.google.com')"
      icon="fa fa-times"
      (iconClick)="visible = false"
    >
      <input type="text">
      <input type="text">
    </ava-card>
    
    <ava-card
      title="Angular"
      marginTop="true"
      buttonLabel="Visit WebSite"
      icon="fa fa-link"
      (buttonClick)="showAlert()"
    >
      <img width="100" src="https://cdn.worldvectorlogo.com/logos/angular-icon-1.svg" alt="">
    </ava-card>
    
     <ava-card
      title="Angular"
      buttonLabel="Visit WebSite"
      icon="fa fa-link"
      (buttonClick)="showAlert()"
    >
      <img width="100" src="https://cdn.worldvectorlogo.com/logos/angular-icon-1.svg" alt="">
    </ava-card>
    
    <ava-hello
      color="green"
      fontSize="10"
      icon="fa fa-user"
      (iconClick)="goto('http://www.adobe.com')"
      username="Mario"
    ></ava-hello>

    <ava-hello
      fontSize="20"
      icon="fa fa-users"
      (iconClick)="goto('http://www.google.com')"
      username="Pippo"
    ></ava-hello>

    <br>
    <hr>

    <ava-card
      *ngFor="let user of users; let i = index; let first = first"
      [title]="user.id + ') ' + user.name"
      icon="fa fa-link"
      (iconClick)="goto(user.link)"
      [marginTop]="!first"
    >
      {{first}}
      {{user.bio}}
      {{user.link}}
    </ava-card>

  `,
})
export class DemoUikit1Component {
  users: any[] = [
    {
      id: 10,
      name: 'Fabio',
      link: 'http://www.fabiobiondi.io',
      bio: 'lorem ipsu,a shfoewhfoewhofewhofwe'
    },
    {
      id: 33,
      name: 'Mario',
      link: 'http://www.mariobiondi.com',
      bio: 'ewfoiew few ohfew hfewlorem ipsu,a shfoewhfoewhofewhofwe'
    },
    {
      id: 23,
      name: 'Pippo',
      link: 'http://www.eohfoewhofhweofew.io',
      bio: 'bla bla lorem ipsu,a shfoewhfoewhofewhofwe'
    }
  ]


  visible = true;
  goto(url: string): void {
    window.open(url)
  }

  showAlert() {
    window.alert('bla bla')
  }
}

