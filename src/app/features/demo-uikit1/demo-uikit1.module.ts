import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DemoUikit1Component } from './demo-uikit1.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [DemoUikit1Component],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: DemoUikit1Component}
    ])
  ]
})
export class DemoUikit1Module { }
