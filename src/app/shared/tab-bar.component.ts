import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ava-tab-bar',
  template: `
    <ul class="nav nav-tabs">
      <li 
        class="nav-item"
        *ngFor="let item of items"
        (click)="tabClick.emit(item)"
      >
        <a 
          class="nav-link"
          [ngClass]="{'active': item.id === active?.id}"
        >
          {{item[labelField]}}
        </a>
      </li>
    </ul>
  `,
})
export class TabBarComponent<T> {
  @Input() items: T[];
  @Output() tabClick = new EventEmitter<T>();
  @Input() active: T;
  @Input() labelField = 'name';
}

