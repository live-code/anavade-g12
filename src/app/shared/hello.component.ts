import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ava-hello',
  template: `
    <div
      [style.color]="color"
      [style.fontSize.px]="fontSize"
      class="hello"
    >
      {{username}}
      <i
        *ngIf="icon"
        style="margin-right: 20px"
        [class]="icon"
        (click)="iconClick.emit()"
      ></i>
    </div>
  `,
})
export class HelloComponent {
  @Input() username = 'Fabio';
  @Input() fontSize = 30;
  @Input() color = 'red';
  @Input() icon: string;
  @Output() iconClick = new EventEmitter();
}

