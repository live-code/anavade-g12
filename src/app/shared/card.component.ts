import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ava-card',
  template: `
    
    <div 
      class="card" 
      [ngClass]="{'mt-2': marginTop}" style="width: 18rem;"
    >
      <h5 
        class="card-header"
        (click)="isOpen = !isOpen"
      >
        {{title}}
        <i
          (click)="iconHandler($event)"
          class="fa pull-right"
          [ngClass]="icon"
        ></i>
      </h5>
      
      
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
      
      <div class="card-footer"  *ngIf="buttonLabel">
        <button
          class="btn btn-primary btn-sm"
          (click)="buttonClick.emit()"
        >{{buttonLabel}}</button>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title: string;
  @Input() buttonLabel: string;
  @Input() icon: string;
  @Input() body: string;
  @Input() marginTop = false;
  @Output() buttonClick = new EventEmitter();
  @Output() iconClick = new EventEmitter();
  isOpen = true;

  iconHandler(event: MouseEvent): void {
    event.stopPropagation();
    this.iconClick.emit();
  }
}
