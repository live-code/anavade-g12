import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ava-gmap',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <img [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + value + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
    <small>{{value}}</small>
    
    {{render()}}
  `,
  styles: [
  ]
})
export class GmapComponent {
  @Input() value: string;

  render(): void {
    console.log('render')
  }
}
