import { NgModule } from '@angular/core';
import { CardComponent } from './card.component';
import { HelloComponent } from './hello.component';
import { TabBarComponent } from './tab-bar.component';
import { GmapComponent } from './gmap.component';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    CardComponent,
    HelloComponent,
    TabBarComponent,
    GmapComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    HelloComponent,
    TabBarComponent,
    GmapComponent,
  ]
})
export class SharedModule {

}
