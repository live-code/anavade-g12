export interface Product {
  id: number;
  price: number;
  desc: string;
  name: string;
}
