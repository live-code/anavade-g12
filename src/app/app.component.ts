import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ava-root',
  template: `
   <ava-navbar></ava-navbar>
   <hr>
   <router-outlet></router-outlet>
  `,
})
export class AppComponent {

}

